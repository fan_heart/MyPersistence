package Fan.MyPersistence.com.fan.demo.use.testImpl;

import java.util.List;
import java.util.Map;

import Fan.MyPersistence.com.fan.demo.use.dao.UseDao;
import Fan.MyPersistence.com.fan.persistence.operation.ExecuteSql;

/** 
* @author liu_fan 2017年3月24日 上午9:18:10  
*/
public class RunText {
	
	public List<Map<String,Object>> getAllList(){
		UseDao dao = new ExecuteSql();
		return dao.getAllInfo("2017");
	}
	
	public static void main(String[] args){
		RunText run = new RunText();
		List<Map<String,Object>> rs = run.getAllList();
		System.out.println("last:"+rs.toString());
	}
}
