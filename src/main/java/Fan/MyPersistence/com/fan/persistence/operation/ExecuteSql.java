package Fan.MyPersistence.com.fan.persistence.operation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Fan.MyPersistence.com.fan.demo.use.dao.UseDao;
import Fan.MyPersistence.com.fan.persistence.database.JDBCConnect;
import Fan.MyPersistence.com.fan.persistence.freemarder.FreeMarker;

/** 
* @author liu_fan 2017年3月23日 下午2:48:41  
*/
public class ExecuteSql implements UseDao{

	@Override
	public List<Map<String, Object>> getAllInfo(String year) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String,Object>>();
		
		//读取freemarker获取sql
		Map<String,Object> root = new HashMap<String,Object>();
        root.put("year", year);
        FreeMarker fm = new FreeMarker("F:\\workspace\\MyPersistence\\src\\main\\java\\Fan\\MyPersistence\\com\\fan\\demo\\use\\sql");
        String sql = fm.getSql(root, "useDao_getAllInfo.sql");
        
		//调用数据库连接进行连接数据库，并获取数据库返回结果
		JDBCConnect jdbcCon = new JDBCConnect();
		
		try {
			mapList = jdbcCon.executeQuery(sql);
			jdbcCon.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mapList;
	}
	
}
