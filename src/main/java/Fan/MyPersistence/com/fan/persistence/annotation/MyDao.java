package Fan.MyPersistence.com.fan.persistence.annotation;
/** 
* @author liu_fan 2017年3月23日 下午2:12:42  
*/

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyDao {
}
