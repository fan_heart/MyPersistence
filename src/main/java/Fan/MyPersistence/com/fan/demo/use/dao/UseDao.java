package Fan.MyPersistence.com.fan.demo.use.dao;
import java.util.*;

import Fan.MyPersistence.com.fan.persistence.annotation.MyDao;
import Fan.MyPersistence.com.fan.persistence.annotation.Params;
/** 
* @author liu_fan 2017年3月23日 下午2:04:15  
*/
@MyDao
public interface UseDao {
	
	@Params({"year"})
	public List<Map<String,Object>> getAllInfo(String year);
}
