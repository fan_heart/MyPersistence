package Fan.MyPersistence.com.fan.persistence.freemarder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateNotFoundException;

/** 
 * @author liu_fan 2017年3月23日 下午4:23:53  
 * 首先创建一个 freemarker.template.Configuration实例，然后调整它的设置。
 * Configuration实例是存储FreeMarker应用级设置的核心部分。
 * 同时，它也处理创建和缓存与解析模板（例：Template对象）的工作
 * 应用级别，只建议使用一次
 * @param className
 * @param strings
 * @return
 */
public class FreeMarker {
	public static Configuration cfg = new Configuration(Configuration.getVersion());
	public FreeMarker(String path){
		try {
			cfg.setDirectoryForTemplateLoading(new File(path));
			cfg.setDefaultEncoding("UTF-8");
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public String getSql(Map<String,Object> root,String templateName){
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			Template temp = cfg.getTemplate(templateName);
			Writer out = new OutputStreamWriter(byteArrayOutputStream);
			temp.process(root, out);
		} catch (TemplateNotFoundException e) {
			e.printStackTrace();
		} catch (MalformedTemplateNameException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
		return byteArrayOutputStream.toString();
	}
}
