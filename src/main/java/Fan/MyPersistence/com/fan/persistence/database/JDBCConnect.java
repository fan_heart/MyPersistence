package Fan.MyPersistence.com.fan.persistence.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** 
* @author liu_fan 2017年3月23日 下午3:30:34  
*/
public class JDBCConnect {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@192.168.1.3:1521/ORCL";
	String username = "zhhq";
	String password = "111111";
	Statement stmt = null;
	ResultSet rs = null;
	Connection conn = null;
	public JDBCConnect(){
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url,username,password);
			stmt = conn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public List<Map<String,Object>> executeQuery(String sql) throws SQLException{
		this.executeQueryRS(sql);
		List<Map<String,Object>> mapList = new ArrayList<Map<String,Object>>();
		ResultSetMetaData md = rs.getMetaData();
		int columnCount = md.getColumnCount();
		while(rs.next()){
			Map rowData = new HashMap();
			for(int i = 1; i <= columnCount; i++){
				rowData.put(md.getColumnName(i), rs.getObject(i));
			}
			mapList.add(rowData);
		}
		return mapList;
	}
	public void executeQueryRS(String sql){
		try {
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public int executeUpdate(String sql){
		int rsCount = 0;
		try {
			rsCount = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsCount;
	}
	public void close(){
		try {
			conn.close();
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
